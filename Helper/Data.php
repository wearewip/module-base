<?php
/**
 * WIP_Base
 *
 * @category    WIP
 * @copyright   Copyright (c) 2019 WIP
 * @author      Vlad Patru <vlad@wearewip.com>
 * @link        http://www.wearewip.com
 */

namespace WIP\Base\Helper;

use Magento\Framework\Module\Dir\Reader;
use SimpleXMLElement;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Response;
use Zend\Uri\Http;
use Magento\Framework\Composer\ComposerInformation;
use Magento\Framework\Xml\Parser;

class Data
{
    const EXT_PATH = 'wip_ext';

    const URL_EXTENSIONS = 'http://feed.migratemagento.com/modules.xml';

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var array|null
     */
    protected $modulesData = null;

    /**
     * @var Reader
     */
    protected $moduleReader;

    /**
     * @var ComposerInformation
     */
    protected $composerInfo;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * Module constructor.
     *
     * @param Reader $moduleReader
     * @param CurlClient $curl
     */
    public function __construct(
        Reader $moduleReader,
        Curl $curl,
        \Magento\Framework\Composer\ComposerInformation $composerInfo,
        Parser $parser
    ) {
        $this->moduleReader = $moduleReader;
        $this->curl = $curl;
        $this->composerInfo = $composerInfo;
        $this->parser = $parser;
    }

    /**
     * Read modules xml
     *
     * @return bool|SimpleXMLElement
     */
    public function getModulesFeed()
    {
        if ($this->curl === null) {
            $this->curl = new curl();
        }

        $curl = $this->curl;

        $location = self::URL_EXTENSIONS;
        $url = new Http($location);

        $curl->connect($url->getHost(), $url->getPort());
        $curl->write('GET', $url, 1.1);
        $content = Response::fromString($curl->read());

        $curl->close();

        $xml = $this->parser->load($location)->xmlToArray();

        return $xml['root'];
    }

    /**
     * @return null
     */
    public function getInstalled()
    {
        $data = $this->composerInfo->getInstalledMagentoPackages();

        return $data;
    }
}
