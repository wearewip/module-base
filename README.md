# WIP Base

The WIP Base Extension is intended to general information about us and about our extensions.

# Changelog
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.8] - 2020-11-25
### Added
Rebranding

## [1.0.7] - 2020-09-15
### Added
Code changes for sidebar

## [1.0.6] - 2019-09-16
### Added
Update side menu icon color integration
## [1.0.5] - 2019-10-11
### Added
- Fix css style and apply new icon

## [1.0.4] - 2019-10-10
### Added
- Update XML code to suit the magento standards

## [1.0.3] - 2019-10-03
### Added
- Updated module readme
- Added WIP information in admin with call to action for contact
- Added installed Extensions section

## [1.0.2] - 2019-08-28
### Added
- Fix action path for admin setting to work on 2.2.x - 2.3.x

## [1.0.1] - 2019-08-27
### Added
- Changed admin menu order to fix infinite loop on 2.2.x versions of Magento

## [1.0.0] - 2019-07-29
### Added
- Install first version of the extension
- README file
