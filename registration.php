<?php
/**
 * WIP_Base
 *
 * @category    WIP
 * @package     WIP_Base
 * @copyright   Copyright (c) 2019 WIP
 * @author      Vlad Patru <vlad@wearewip.com>
 * @link        http://www.wearewip.com
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'WIP_Base',
    __DIR__
);
