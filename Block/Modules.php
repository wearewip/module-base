<?php
/**
 * WIP_Base
 *
 * @category    WIP
 * @copyright   Copyright (c) 2019 WIP
 * @author      Vlad Patru <vlad@wearewip.com>
 * @link        http://www.wearewip.com
 */

namespace WIP\Base\Block;

use WIP\Base\Helper\Data;
use Magento\Backend\Block\Context;
use Magento\Backend\Model\Auth\Session;
use Magento\Config\Block\System\Config\Form\Fieldset;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\View\Helper\Js;
use Magento\Framework\View\LayoutFactory;

/**
 * Class Modules | Block for Installed Extensions in admin
 **/
class Modules extends Fieldset
{
    const IMG_PATH = 'WIP_Base::images/';

    const UPD = 'update.svg';

    const INST = 'installed.svg';

    const DISB = 'error.svg';

    /**
     * @var ModuleListInterface
     */
    protected $moduleList;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var Data
     */
    protected $moduleHelper;

    /**
     * Modules constructor.
     *
     * @param Context $context
     * @param Session $authSession
     * @param Js $jsHelper
     * @param ModuleListInterface $moduleList
     * @param LayoutFactory $layoutFactory
     * @param Data $moduleHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $authSession,
        Js $jsHelper,
        ModuleListInterface $moduleList,
        LayoutFactory $layoutFactory,
        Data $moduleHelper,
        array $data = []
    ) {
        parent::__construct($context, $authSession, $jsHelper, $data);

        $this->moduleList = $moduleList;
        $this->layoutFactory = $layoutFactory;
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * Render fieldset html
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $html = $this->_getHeaderHtml($element);

        $feed = $this->moduleHelper->getModulesFeed();

        foreach ($feed as $moduleName => $values) {
            if (strstr($moduleName, 'WIP_') === false
                || $moduleName === 'WIP_Base'
            ) {
                continue;
            }
            $html .= $this->getFieldHtml($element, $moduleName);
        }

        $html .= $this->_getFooterHtml($element);

        return $html;
    }

    /**
     * @return \Magento\Framework\View\Element\BlockInterface
     */
    protected function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $this->_fieldRenderer = $this->_layout->createBlock(
                \Magento\Config\Block\System\Config\Form\Field::class
            );
        }

        return $this->_fieldRenderer;
    }

    /**
     * @param $fieldset
     * @param $moduleCode
     *
     * @return string
     */
    public function getFieldHtml($fieldset, $moduleCode)
    {
        $module = $this->moduleHelper->getModulesFeed();

        $moduleSlug = $module[$moduleCode]["slug"];
        $moduleUrl = $module[$moduleCode]["url"];
        $allExtensions = $this->moduleHelper->getInstalled();
        $status = $this->getImghtml(self::INST, 'Installed');

        $version = str_replace('v', '', $module[$moduleCode]["tag"]);
        $moduleName = $moduleCode;

        if ($allExtensions && isset($allExtensions[$moduleSlug])) {
            $singleRecord = array_key_exists('name', $allExtensions[$moduleSlug]);
            $ext = $singleRecord ? $allExtensions[$moduleSlug] : end($allExtensions[$moduleSlug]);

            $name = $ext['name'];
            $lastVer = $ext['version'];

            if (version_compare($version, $lastVer, '>')) {
                $status = $this->getImghtml(self::UPD, 'Update available');
            }
            $versions = 'Current Version: ' .$lastVer. ' || Latest version: '. $version;
        }

        if (!isset($allExtensions[$moduleSlug])) {
            $versions = 'Current Version: none || Latest version: '. $version;
            $status = $this->getImghtml(self::DISB, 'Not installed');
        }

        $statusContent = $moduleName . ' ' . $status ;
        if ($moduleUrl && $moduleUrl !== '-') {
            $moduleField = '<a href="'.$moduleUrl.'" target="_blank" style="color: inherit">'.$statusContent.'</a>';
        } else {
            $moduleField = $moduleName . ' ' . $status;
        }

        $field = $fieldset->addField(
            $moduleCode,
            'label',
            [
                'name' => $status,
                'label' => $moduleField,
                'value' => $versions
            ]
        )->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }

    /**
     * @param $imgName
     * @param $title
     * @return mixed | Image html for admin section
     */
    public function getImghtml($imgName, $title)
    {
        $img = self::IMG_PATH . $imgName;

        $imgHtml = '<img src="'. $this->getViewFileUrl($img) .'" width="19" height="19" style="vertical-align: middle;" alt="'. __($title) .'" title="'. __($title) .'"/>';

        return $imgHtml;
    }
}
