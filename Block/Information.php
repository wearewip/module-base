<?php
/**
 * WIP_Base
 *
 * @category    WIP
 * @copyright   Copyright (c) 2019 WIP
 * @author      Vlad Patru <vlad@wearewip.com>
 * @link        http://www.wearewip.com
 */

namespace WIP\Base\Block;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

/**
 * Class Information | Show html information in admin block
 */
class Information extends Field
{

    /**
     * Template path
     *
     * @var string
     */
    protected $_template = 'WIP_Base::information.phtml';

    public function render(AbstractElement $element)
    {
        $columns = $this->getRequest()->getParam('website') || $this->getRequest()->getParam('store') ? 5 : 4;
        return $this->_decorateRowHtml($element, "<td colspan='{$columns}'>" . $this->toHtml() . '</td>');
    }
}
